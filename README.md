# Archivage

Projet de [OpenXtrem](https://www.openxtrem.com/).

Client d'archivage permettant l'ajout et la consultation de données médicales pour des établissements utilisant ***Mediboard***.

## Start
```bash
    docker-compose up
```

## Stop and remove container
```bash
    docker-compose stop
    docker-compose rm -f
```

## Web
+ website : localhost:88
+ phpmyadmin : localhost:8080
    - login : root
    - password : root
    
## Autres
```bash
    bcdedit /set hypervisorlaunchtype off
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```