<?php
session_start();
unset($_SESSION['user']);
unset($_SESSION['role']);
$_SESSION['flash']['success'] = "Vous êtes maintenant déconnectés";
header('Location: index.php');
