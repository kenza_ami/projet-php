
<?php
if(!empty($_POST)){
    $app = App::getInstance();
    $user = $app->getTable('personne')->login($_POST['email'], $_POST['password']);
    if($user) {
        if($_SESSION['role'] == "client") {
            header('location: index.php?p=accueil');
        } elseif ($_SESSION['role'] == "admin") {
            header('location: index.php?p=admin');
        }

    } else {
       $_SESSION['flash']['danger'] = "Identifiants incorrects";
    }
}
?>
<link rel="stylesheet" href="css/style.css">
<div class="row">
    <div class="col-sm-6 main-section">
        <div class="modal-content">
            <div class="col-12 user-img">
                <img src="img/face.png" >
            </div>

            <div class="col-12 form-input">
                <form method="Post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Entrer votre identifiant" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Entrer votre mot de passe" required>
                    </div>
                    <button t ype="submit" class="btn btn-success" name="formconnexion">Connexion</button>
                </form>

            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="../../public/css/style.css">
<style></style>