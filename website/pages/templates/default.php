<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title><?= App::getInstance()->getTitle();?></title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-fixed/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <?php if(isset($_SESSION['user'])): ?>
        <a class="navbar-brand" href="?p=accueil">OX Archivage</a>
    <?php endif; ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <?php if(isset($_SESSION['user']) and $_SESSION['role'] != "admin"): ?>
                <li class="nav-item">
                    <a class="nav-link" href="?p=recherche">Recherche <span class="sr-only">(current)</span></a>
                </li>
            <?php endif;?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php if(isset($_SESSION['user'])): ?>
                <li class="nav-item active"><a class="nav-link" href="?p=logout">Se déconnecter</a></li>
            <?php else: ?>
                <li class="nav-item"><a class="nav-link" href="?p=login">Se connecter</a></li>
            <?php endif;?>
        </ul>

    </div>
</nav>

<main role="main" class="container">
    <div class="starter-template" style="padding-top: 100px;">
        <?= $content; ?>
    </div>
</main>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
<script src="js/bootstrap.js"></script>

<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.15.5/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.15.5/dist/bootstrap-table-locale-all.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function(){
        $('.close').click(function(){
            $(this).parent().slideUp(2000);
        });

        $('#users').DataTable( {
            "language": {
                "lengthMenu": "Nombre affichages _MENU_ enregistrements par page",
                "zeroRecords": "Pas de résultat trouvés - désolé",
                "info": "Résultats _PAGE_ sur _PAGES_",
                "infoEmpty": "Pas de résultats correspondant",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "search":         "Recherche:",
                "paginate": {
                    "first":      "Début",
                    "last":       "Dernier",
                    "next":       "Suivant",
                    "previous":   "Précédent"
                }
            }
        } );
    });
</script>
</body>
</html>
