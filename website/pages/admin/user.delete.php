<?php
$app = App::getInstance();
if(!isset($_SESSION['user']) or !isset($_SESSION['role']) or $_SESSION['role'] != "admin") {
    $app->forbidden();
}

$personneTable = $app->getTable('personne');
$user = $personneTable->getUser($_GET['id']);

if(!$user){
     $app->notFound();
}
if (!empty($_POST)){

    $res = $personneTable->deleteUser($_POST['id']);
    if($res) {
       // session_start();
        $_SESSION['flash']['success'] = "L'utilisateur a été supprimé avec succès";
        var_dump($_SESSION);
        header('Location: index.php?p=admin');
    }
}

?>