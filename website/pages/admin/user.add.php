<?php
$app = App::getInstance();
if(!isset($_SESSION['user']) or !isset($_SESSION['role']) or $_SESSION['role'] != "admin") {
    $app->forbidden();
}
$table = $app->getTable('personne');
$roles = $app->getTable('role')->all();

if (!empty($_POST)) {
    $errors = array();
    if(empty($_POST['email'])) {
        $errors['email'] = "Veuillez rentrer l'email";
    } elseif ( !filter_var($_POST['email'])) {
        $errors['email'] = "L'email rentré n'est pas valide";
    } else {
        $user = $table->getUser($_POST['email']);
        if ($user) {
            $errors['email'] = "Ce email existe déjà";
        }
    }
    if (empty($_POST['nom']) | !preg_match('/^[A-Za-zÀ-ÖØ-öø-ÿ_\s-]+$/', $_POST['nom'])) {
        $errors['nom'] = "Vérifier le nom insérer";
    }
    if (empty($_POST['prenom']) | !preg_match('/^[A-Za-zÀ-ÖØ-öø-ÿ_\s-]+$/', $_POST['prenom'])) {
        $errors['prenom'] = "Vérifier le prénom insérer";
    }
    if(empty($_POST['motdepasse'])){
        $errors['motdepasse'] = "Le mot de passe est vide";
    } elseif (!preg_match('/[-\'0-9a-zA-Z]{4,50}/', $_POST['motdepasse'])) {
        $errors['motdepasse'] = "Le mot de passe n'est pas valide";
    } elseif ($_POST['motdepasse'] != $_POST['confirm_password']) {
        $errors['motdepasse'] = "Les mots de passe ne sont pas identiques";
    }

    if (empty($_POST['date_naissance'])) {
        $errors['date_naissance'] = "Veuiilez mettre la date de naissance";
    } else {
        $now = date('Y-m-d');
        if ($now < $_POST['date_naissance']) {
            $errors['date_naissance'] = "La date de naissance est supérieure à la date d'ajourd'hui";
        }
    }

    if (empty($_POST['adresse'])) {
        $errors['adresse'] = "Veuillez mettre l'adresse";
    }

    if (empty($errors)) {
        $motdepasse = password_hash($_POST['motdepasse'], PASSWORD_BCRYPT);
        $result = $table->newUser($_POST['nom'], $_POST['prenom'], $_POST['date_naissance'], $_POST['email'], $_POST['adresse'], $motdepasse, $_POST['role']);
        if($result) {
            session_start();
            $_SESSION['flash']['success'] = "L'utilisateur a été ajouté avec succès";
            header('Location: index.php?p=admin');
        }
    }
}
?>
<?php if(!empty($errors)) :?>
    <div class="alert alert-danger">
        <p>Vous n'avez pas rempli le formulaire correctement</p>
        <a href="#" class="close">x</a>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>
        </ul>

    </div>
<?php endif; ?>

<h1 style="text-align: center">Ajout d'un nouvel utilisateur</h1>

<form action="" method="post">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Nom</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="" placeholder="Nom" name="nom">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Prénom</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="" placeholder="Prénom" name="prenom">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Mot de passe</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="motdepasse">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Confirmation(mot de passe)</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword3" placeholder="Confirmation mot de passe" name="confirm_password">
        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Date de naissance</label>
        <div class="col-sm-10">
            <input type="date" class="form-control" id="" placeholder="Date de naissance" name="date_naissance">
        </div>
    </div>
    <div class="form-group row">
        <label for="" class="col-sm-2 col-form-label">Adresse</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="" placeholder="adresse" name="adresse">
        </div>
    </div>
    <div class="frm-group">
        <label for="" class="col-sm-2 col-form-label">Statut</label>
        <select name="role" id="" class="form-control">
            <?php foreach($roles as $role): ?>
                <option value="<?= $role['idrole'] ;?>"><?= $role['intitule'] ;?></option>
            <?php endforeach;?>
        </select>
    </div>

    <div class="form-group row" style="padding-top: 10px;">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Ajouter</button>
            <a href="?p=admin" class="btn btn-primary">Annuler</a>
        </div>
    </div>
</form>