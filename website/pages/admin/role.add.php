<?php
$app = App::getInstance();
if(!isset($_SESSION['user']) or !isset($_SESSION['role']) or $_SESSION['role'] != "admin") {
    $app->forbidden();
}
if (!empty($_POST)) {
$errors = array();
if(empty($_POST['role'])) {
    $errors['role'] = "Veuillez rentrer l'intitulé du rôle";
}
if (empty($errors)) {
    $result = $app->getTable('role')->addRole($_POST['role']);
    if ($result) {
        session_start();
        $_SESSION['flash']['success'] = "Le status est ajouté avec succès";
        header('Location: index.php?p=admin');
    }
}

}
?>
<?php if(!empty($errors)) :?>
    <div class="alert alert-danger">
        <p>Vous n'avez pas rempli le formulaire correctement</p>
        <a href="#" class="close">x</a>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>
        </ul>

    </div>
<?php endif; ?>
<h1>Ajout d'un rôle</h1>


<form action="" method="post">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Intitulé du rôle</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="role" placeholder="rôle" name="role">
        </div>
    </div>
    <div class="form-group row" style="padding-top: 10px;">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Ajouter</button>
            <a href="?p=admin" class="btn btn-primary">Annuler</a>
        </div>
    </div>
</form>