<?php
$app = App::getInstance();
if(!isset($_SESSION['user']) or !isset($_SESSION['role']) or $_SESSION['role'] != "admin") {
    $app->forbidden();
}
$users = $app->getTable('personne')->getUsers();
?>

<?php if (isset($_SESSION['flash'])):?>
    <?php foreach ($_SESSION['flash'] as $type=>$message): ?>
        <div class="alert alert-<?= $type; ?>">
            <a href="#" class="close">x</a>
            <?= $message; ?>
        </div>
    <?php endforeach;
    unset($_SESSION['flash']);
endif;?>

<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,500" rel="stylesheet"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
<h1>Administration</h1>
<div class="row">
    <p style="margin-right: 10px;"><a href="?p=user.add" class="btn btn-primary">Ajouter nouvel utilisateur</a></p>
    <p><a href="?p=role.add" class="btn btn-success">Ajouter nouveau statut</a></p>

</div>

<table class="table table-striped" id="users">
    <thead>
        <tr>
            <td>Email</td>
            <td>Nom</td>
            <td>Prénom</td>
            <td>Statut</td>
            <td>Dernière connexion</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
    <?php foreach($users as $user): ?>
        <tr>
            <td><?= $user->email ;?></td>
            <td><?= $user->nom ;?></td>
            <td><?= $user->prenom ;?></td>
            <td><?= $user->statut ;?></td>
            <td><?= $user->derniere_connexion ;?></td>
            <td>
                <a type="button" class="btn btn-primary" href="?p=user.edit&id=<?= $user->email;?>">
                   éditer
                </a>
                <form action="?p=user.delete" method="post" style="display: inline" onsubmit="return confirm('Êtes-vous vriament sûr?')">
                    <input type="hidden" name="id" value="<?= $user->email;?>">
                    <button type="submit" href="?p=user.delete&id=<?= $user->email;?>" class="btn btn-danger">Supprimer</button>
                </form>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>