<?php

define('ROOT', dirname(__DIR__));
require ROOT . '/app/App.php';

App::load();


if (isset($_GET['p'])) {
    $p = $_GET['p'];
} else {
    $p = 'accueil';
}

ob_start();
if($p === 'login') {
    require ROOT . '/pages/users/login.php';
} elseif($p === 'accueil') {
    require ROOT . '/pages/users/home.php';
}elseif($p === 'logout') {
    require ROOT . '/pages/users/logout.php';
} elseif($p === 'recherche') {
    require ROOT . '/pages/users/recherche.php';
} elseif($p === 'admin') {
    require ROOT . '/pages/admin/admin.php';
} elseif($p === 'user.add') {
    require ROOT . '/pages/admin/user.add.php';
} elseif($p === 'user.edit') {
    require ROOT . '/pages/admin/user.edit.php';
} elseif($p === 'user.delete') {
    require ROOT . '/pages/admin/user.delete.php';
} elseif($p === 'role.add') {
    require ROOT . '/pages/admin/role.add.php';
}
$content = ob_get_clean();
require ROOT . '/pages/templates/default.php';

