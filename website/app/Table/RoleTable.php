<?php


namespace App\Table;


use Ap\Entity\RoleEntity;
use Core\Table\TableFactory;

class RoleTable extends TableFactory {
    protected $table = 'role';

    /**
     * @return mixed
     */
    public function getRoles() {
        return $this->query("
            SELECT R.idrole, R.intitule FROM role R 
         ");
    }

    /**
     * @param $role
     * @return mixed
     */
    public function addRole($role) {
        return $this->create([
            "intitule" => $role
        ]);
    }
}