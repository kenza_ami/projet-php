<?php
namespace App\Table;
use App\Entity\PersonneEntity;
use Core\Table\TableFactory;
use \DateTime;
class PersonneTable extends TableFactory {
    protected $table = 'personne';

    /**
     * @param $email
     * @return mixed
     */
    public function getUser($email) {
        return $this->query("
            SELECT U.idclient, U.email, U.motdepasse, U.nom, U.prenom, U.date_naissance, U.adresse, R.idrole, R.intitule
            FROM personne U, role R
            WHERE U.email = ? and U.role_idrole = R.idrole
        ", [$email], $this, true);

    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function login($email, $password) {
        $user = $this->query("
        SELECT U.idclient, U.email, U.motdepasse, U.nom, U.prenom, R.idrole, R.intitule 
        FROM personne U, role R
        WHERE U.email = ? and U.role_idrole = R.idrole
        ", [$email], true);

        if($user){
            if(password_verify($password, $user->motdepasse))
            {

                $_SESSION['user'] = $user->email;
                $_SESSION['role'] = $user->intitule;
                $this->updateHistory($user->email);
                $_SESSION['flash']['success'] = "Vous êtes maintenant connectés";
                //$_SESSION['role'] = $user->intitule;
                return true;
            }

        }
        return false;
    }

    public function getUsers() {
        return $this->query("
            SELECT P.idClient, P.email, P.motdepasse, P.nom, P.prenom, P.derniere_connexion, R.intitule as statut
            FROM personne P
            LEFT JOIN role R
                ON P.role_idrole = R.idrole
        ");
    }

    /**
     * @param $email
     * @param $nom
     * @param $prenom
     * @param $motdepasse
     * @param $statut
     * @return mixed
     */
    public function newUser($nom, $prenom, $date_naissance, $email, $adresse, $motdepasse, $statut) {
        return $this->create([
            "nom" => $nom,
            "prenom" => $prenom,
            "date_naissance" => $date_naissance,
            "email" => $email,
            "adresse" => $adresse,
            "motdepasse" => $motdepasse,
            "role_idrole" => $statut
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateUser($email, $nom, $prenom, $motedepasse, $date_naissance, $adresse, $role) {
       return $this->update($email, [
           'nom' => $nom,
           'prenom' => $prenom,
           'motdepasse' => $motedepasse,
           'date_naissance' => $date_naissance,
           'adresse' => $adresse,
           'role_idrole' => $role

       ]);
    }

    /**
     * @param $email
     * @return mixed
     * @throws \Exception
     */
    public function updateHistory($email) {
        $date = new DateTime('now');
        return $this->update($email, [
            'derniere_connexion' => $date->format('d-m-Y H:i:s')
        ]);
    }


}